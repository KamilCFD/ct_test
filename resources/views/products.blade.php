<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CT</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="container">
	<h2>Product Editor</h2>
	<form name="sellerForm" class="form-horizontal" role='form'>
	    <div class="form-group">
	        <label class="control-label col-sm-2">Product Name:</label>
	        <div class="col-sm-10">
	        	<input id="prodName" type="text" class="prodName" >
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="control-label col-sm-2">Quantity in stock:</label>
	        <div class="col-sm-10">
	        	<input type="text" class="stock">
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="control-label col-sm-2">Price per item:</label>
	        <div class="col-sm-10">
		        <input type="text" class="price" >
		    </div>
	    </div>
	    <div class="form-group">
			<div class="col-sm-5">
	        	<input type="button" value="SAVE" onclick="save()" class="btn pull-right">
	        </div>

	    </div>
	    <input type="hidden" class="id" value="" >

	    </p>
    </form>
</div>

<div class="container">
	<h2>Table</h2>
    <table class='table'>
    </table>
</div>

	<!-- Scripts -->
	<script>
		var allData = []
		var allDataIdToRow = []
		function loadData() {
			$.get( "{{url('load')}}", {}, function( data ) {
				var sorttmp = []
				var sortdates = []
				$.each(data, function (idx,row) {
					sorttmp[row['datetime']] = idx
					sortdates.push(row['datetime'])
				})

				sortdates.sort().reverse();
				allData = []
				$.each(sortdates, function (idx,val) {
					allData.push(data[sorttmp[val]])
				})
				allDataIdToRow = data

				$(".table").empty();
				$('.table').append("<tr><td>Product Name</td><td>Quantity in stock</td><td>Price per item</td><td>Datetime Submited</td><td>Total Value</td><td>Edit</td></tr>")

				totalTotal = 0
				$.each(allData, function (idx,row) {
					totalTotal = totalTotal + row['total']
					$('.table tr:last').after(
						'<tr><td>' + row['prodName'] + '</td>' +
						'<td>' + row['stock'] + '</td>' +
						'<td>' + row['price'] + '</td>' +
						'<td>' + row['datetime'] + '</td>' +
						'<td>' + row['total'] + '</td>' +
						'<td>' + '<input name="edit_' + row['id'] + '" type="button" value="edit" class="btn" onclick="edit(' + row['id'] + ')"></td></tr>'
									)
				})

				$('.table tr:last').after('<tr><td></td><td></td><td></td><td></td><td>' + totalTotal + '</td><td></td></tr>')

			})
		}

		loadData()

		function save() {
			var prodName = $('input.prodName').val()
			var stock = $('input.stock').val()
			var price = $('input.price').val()
			var id = $('input.id').val()

			$.get( "{{url('save')}}", {	'prodName' : prodName,
												'stock': stock,
												'price': price,
												'id' : id
											}, function( data ) {

				loadData();
			})
		}

		function edit(id) {
			$('input.prodName').val(allDataIdToRow[id]['prodName'])
			$('input.stock').val(allDataIdToRow[id]['stock'])
			$('input.price').val(allDataIdToRow[id]['price'])
			$('input.id').val(id)
		}



	</script>

</body>
</html>
