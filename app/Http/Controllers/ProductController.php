<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Input;

class ProductController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
     */

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function getIndex()
    {
        return view('products');
    }

    public function getLoad()
    {
        $file = storage_path() . "/data.json";
        $data = [];
        if (file_exists($file))
        {
            $string = file_get_contents($file);
            $data = json_decode($string, true);
        }
        return $data;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function getSave()
    {

        $prodName = Input::get('prodName');
        $stock = Input::get('stock');
        $price = Input::get('price');

        $total = $stock * $price;
        $id = Input::get('id');

        $row = ['id' => $id, 'prodName' => $prodName, 'stock' => $stock, 'price' => $price, 'datetime' => date("Y-m-d H:i:s"), 'total' => $total];
        $file = storage_path() . "/data.json";

        $data = [];
        if (file_exists($file))
        {
            $string = file_get_contents($file);
            $data = json_decode($string, true);
        }

        if ($id == null || $id == '')
        {
            $id = count($data) + 1;
        }

        $row['id'] = $id;
        $data[$id] = $row;

        $fp = fopen($file, 'w');
        fwrite($fp, json_encode($data));
        fclose($fp);

        return 'ok';
    }

}
